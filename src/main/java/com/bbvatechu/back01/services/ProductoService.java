package com.bbvatechu.back01.services;


import com.bbvatechu.back01.components.AuthHandler;
import com.bbvatechu.back01.models.ProductoModel;
import com.bbvatechu.back01.repositories.ProductRepository;
import org.jose4j.http.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    Logger logger = LoggerFactory.getLogger(AuthHandler.class);

    @Autowired
    ProductRepository productRepository;

    public List<ProductoModel> findAll(){
        return productRepository.findAll();
    }

    public Optional<ProductoModel> findById(String id){
        return productRepository.findById(id);
    }

    public Optional<ProductoModel> findOne(String id){
        return productRepository.findById(id);
    }

    public ResponseEntity<ProductoModel> save(ProductoModel producto, Boolean create){
        try {
            ProductoModel pmcreateupdate = productRepository.save(producto);
            if(create) {
                return new ResponseEntity<>(pmcreateupdate,HttpStatus.OK);
            }
            else
            {
                return new ResponseEntity<>(pmcreateupdate,HttpStatus.CREATED);
            }
        }
        catch (Exception e)
        {
            logger.error("ERROR EN EL SAVE " + e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public boolean existById(String id){
        return productRepository.existsById(id);
    }



    public boolean delete(ProductoModel producto){
        try{
            productRepository.delete (producto);
            return true;
        }
        catch(Exception e)
        {
            System.out.println("******* " + e.toString());
            return false;
        }
    }

    public boolean deleteById(String id){
        try{
            productRepository.deleteById(id);
            return true;
        }
        catch(Exception e)
        {
            System.out.println("******* " + e.toString());
            return false;
        }
    }


}
