package com.bbvatechu.back01.repositories;


import com.bbvatechu.back01.models.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<ProductoModel,String> {
}
