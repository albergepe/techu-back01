package com.bbvatechu.back01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@SpringBootApplication
@ComponentScan(basePackages = {"com.kastkode.springsandwich.filter","com.bbvatechu.back01"})
public class Back01Application {

	public static void main(String[] args) {
		SpringApplication.run(
				Back01Application.class, args);
	}
}
