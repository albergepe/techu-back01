package com.bbvatechu.back01.controllers;


import com.bbvatechu.back01.components.AuthHandler;
import com.bbvatechu.back01.components.JWTBuilder;
import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import com.bbvatechu.back01.models.ProductoModel;
import com.bbvatechu.back01.services.ProductoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.PATCH})
@RequestMapping("${base.uri}")
public class ProductoController {

    @Autowired
    JWTBuilder jwtBuilder;

    @Autowired
    ProductoService productoService;

    Logger logger = LoggerFactory.getLogger(AuthHandler.class);

    @GetMapping()
    public String index() {
        return "Página principal";
    }


    @GetMapping("/gettoken")
    public String tokenget(@RequestParam(value="nombre", defaultValue="Tech U!") String name){
        return jwtBuilder.generateToken(name,"admin");
    }


    @GetMapping("/productos")
    public List<ProductoModel> getProductos(){
        return productoService.findAll();
    }


    @GetMapping(path="/productos/{id}",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public ResponseEntity getProductoById(@PathVariable String id){
        Optional<ProductoModel> optionalProduct = productoService.findById(id);
        if(optionalProduct.isPresent())
        {
            return ResponseEntity.ok(optionalProduct.get());
        }
        else
        {
             return new ResponseEntity<>("Elemento " + id + " no encontrado",HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping(path="/productos",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public ResponseEntity<ProductoModel> postProducto(@RequestBody ProductoModel newProduct){
         return productoService.save(newProduct,true);
    }


    @PutMapping("/productos")
    public ResponseEntity<ProductoModel> putProducto(@RequestBody ProductoModel upProduct){
        return new ResponseEntity(HttpStatus.FORBIDDEN);
    }

    @PutMapping(path="/productos/{id}",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public ResponseEntity putProductoById(@RequestBody ProductoModel upProduct){


        if(productoService.findById(upProduct.getId()).isPresent())
        {
            return productoService.save(upProduct,false);
        }
        else{
            return new ResponseEntity<>("Elemento no encontrado", HttpStatus.NOT_FOUND);
        }


    }

    @DeleteMapping("/productos")
    public ResponseEntity deleteProducto(@RequestBody ProductoModel delProduct){
       return new ResponseEntity(HttpStatus.FORBIDDEN);

    }
    @DeleteMapping(path="/productos/{id}",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public ResponseEntity deleteProductoById(@PathVariable String id){
        if(productoService.existById(id)) {
            if(productoService.deleteById(id))
            {
                return new ResponseEntity<>("Borrado correcto del elemento con id " + id, HttpStatus.OK);
            }
            else
            {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
        else{
            return new ResponseEntity<>("Elemento " + id + " no encontrado", HttpStatus.NOT_FOUND);
        }

    }

    @PatchMapping(path="/productos/{id}",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public ResponseEntity patchProducto(@PathVariable String id,@RequestBody String description)
    {
        Optional<ProductoModel> optionalProduct = productoService.findById(id);
        if(productoService.existById(id)) {
           optionalProduct.get().setDescripcion(description);
            return new ResponseEntity<>(optionalProduct.get(), HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>("Elemento " + id + " no encontrado", HttpStatus.NOT_FOUND);
        }
    }
}
