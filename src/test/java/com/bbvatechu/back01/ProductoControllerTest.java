package com.bbvatechu.back01;

import com.bbvatechu.back01.controllers.ProductoController;
import com.bbvatechu.back01.models.ProductoModel;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductoControllerTest
{
    ProductoModel pmodel= new ProductoModel();
    @Test
    public void testCaseDeleteMethodWithoutId()
    {
        ProductoController pctroller = new ProductoController();
        assertEquals(new ResponseEntity<>(HttpStatus.FORBIDDEN),pctroller.deleteProducto(pmodel));
    }
}
